package exerciciosExcecoes;

public class SaldoInsuficienteException extends Exception{

     public SaldoInsuficienteException() {
          super("Saldo insufisciente para o saque solicitado!");
     }
     
}
