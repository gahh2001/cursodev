package exerciciosExcecoes;

public class ContaNaoEcontradaException extends Exception{

     public ContaNaoEcontradaException() {
          super("Conta não encontrada para o CPF");
     }
     
}
