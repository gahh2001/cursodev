package exerciciosExcecoes;

public class SaldoNegativadoException extends Exception{

     public SaldoNegativadoException() {
          super("O saldo do correntista está negativado");
     }
}
