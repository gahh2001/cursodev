package exerciciosExcecoes;

public class Conta {
     
     private String nome;
     private String cpf;
     private String banco;
     private int agencia;
     private int conta;
     private Double saldo;

     public Conta(String nome, String cpf, String banco, int agencia, int conta, Double saldo) {
          this.nome = nome;
          this.cpf = cpf;
          this.banco = banco;
          this.agencia = agencia;
          this.conta = conta;
          this.saldo = saldo;
     }
     
     @Override
     public String toString() {
          return "Conta [agencia=" + agencia + ", banco=" + banco + ", conta=" + conta + ", cpf=" + cpf + ", nome="
                    + nome + ", saldo=" + saldo + "]";
     }

     public Double getSaldo() {
          return saldo;
     }
     public void setSaldo(Double saldo) {
          this.saldo = saldo;
     }
     
     public String getNome() {
          return nome;
     }
     public void setNome(String nome) {
          this.nome = nome;
     }
     public String getCpf() {
          return cpf;
     }
     public void setCpf(String cpf) {
          this.cpf = cpf;
     }
     public String getBanco() {
          return banco;
     }
     public void setBanco(String banco) {
          this.banco = banco;
     }
     public int getAgencia() {
          return agencia;
     }
     public void setAgencia(int agencia) {
          this.agencia = agencia;
     }
     public int getConta() {
          return conta;
     }
     public void setConta(int conta) {
          this.conta = conta;
     }

}
