package exerciciosExcecoes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Main {
     public static void main(String[] args) {
          
          List<Conta> contas1 = new ArrayList<>();

          contas1.add(new Conta("Gabriel G.", "25164985004", "Nubank", 854, 98354154, 50.00));
          contas1.add(new Conta("Natalia G.", "46431928016", "Inter", 456, 69821547, 100.00));
          contas1.add(new Conta("Pedro M.", "05953377088", "Banrisul", 279, 97501577, -27.00));
          contas1.add(new Conta("Ana O.", "52147016080", "Nubank", 716, 30246798, -100.00));
          contas1.add(new Conta("Julio I.", "57200842079", "PicPay", 516, 19467356, 65.00));
          contas1.add(new Conta("Fred D.", "64163363084", "Bradesco", 825, 49568315, 0.0));
          contas1.add(new Conta("Davi J.", "42152339096", "Picpay", 462, 49738205, 98.65));
          contas1.add(new Conta("Rafael H.", "06010798094", "Inter", 815, 46051278, 0.50));
          contas1.add(new Conta("Bruno F.", "16761034060", "Itau", 642, 80210465, 12000.00));
          contas1.add(new Conta("Maria N.", "84618008008", "Itau", 854, 940561841, 500.00));
          contas1.add(new Conta("Gustavo B.", "33857036036", "Bradesco", 985, 500134659, 84.65));

          System.out.println("-----------------------------------");
          System.out.println("Consultando CPF...");
          try {
               System.out.println(consultaCPF(contas1, "42152339096"));
               System.out.println(consultaCPF(contas1, "00000000000"));
          } catch (ContaNaoEcontradaException e) {
               System.out.println(e.getMessage());
          }

          System.out.println("-----------------------------------");
          System.out.println("Consultando Saldo...");
          try {
               System.out.println(buscaSaldo(contas1, "42152339096"));
               System.out.println(buscaSaldo(contas1, "05953377088"));
          } catch (SaldoNegativadoException e) {
               System.out.println(e.getMessage());    
          }

          System.out.println("-----------------------------------");
          System.out.println("Solicitando saque...");
          try {
               saque(contas1, "Bradesco", 985, 500134659, 50.00);
               saque(contas1, "Itau", 642, 80210465, 20000.00);
          } catch (ContaNaoEcontradaException e) {
               System.out.println(e.getMessage());
          } catch (SaldoInsuficienteException s) {
               System.out.println(s.getMessage());
          }
     }

     public static List<Conta> consultaCPF(List<Conta> contas, String cpf) throws ContaNaoEcontradaException{
          List<Conta> comCPF = contas.stream()
                              .filter(conta -> conta.getCpf().equals(cpf))
                              .collect(Collectors.toList());
          if (comCPF.size() < 1) {
               throw new ContaNaoEcontradaException();
          } else {
               return comCPF;
          }
          
     }

     public static List<Double> buscaSaldo(List<Conta> contas, String cpf) throws SaldoNegativadoException{
          List<Double> saldo = contas.stream()
                              .filter(conta -> conta.getCpf().equals(cpf))
                              .filter(conta -> (conta.getSaldo() >= 0))
                              .flatMap(conta -> Arrays.asList(conta.getSaldo()).stream())
                              .collect(Collectors.toList());
          if (saldo.size() < 1) {
               throw new SaldoNegativadoException();
          } else {
               return saldo;
          }
     }

     public static void saque(List<Conta> contas, String banco, int agencia, int conta, Double saqueSolicitado) throws ContaNaoEcontradaException, SaldoInsuficienteException{
          Optional<Conta> contaIdentificada = contas.stream()
                    .filter(cliente -> cliente.getBanco().equals(banco))
                    .filter(cliente -> cliente.getAgencia() == agencia)
                    .filter(cliente -> cliente.getConta() == conta)
                    .findFirst();
          if (!contaIdentificada.isPresent()) {
               throw new ContaNaoEcontradaException();
          } else {
               System.out.println("Conta identificada: " + contaIdentificada);
               Double saldoDaContaEncontrada = contaIdentificada.get().getSaldo();
               if (saldoDaContaEncontrada < saqueSolicitado){
                    throw new SaldoInsuficienteException();
               } else { 
                    System.out.println("Saque realizado com sucesso!\nSaldo atual: " + String.format("%.2f", saldoDaContaEncontrada - saqueSolicitado)); 
               }
          }
          
     }
}
