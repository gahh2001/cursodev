package exerciciosStream;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
public class Veiculo {
     
	private String marca;
	private String modelo;
	private String placa;
	private String chassi;
	private int valor;
	
	@Override
	public String toString() {
	     return "\nVeiculo [chassi=" + chassi + ", marca=" + marca + ", modelo=" + modelo + ", placa=" + placa + ", valor="
	               + valor + "]";
	}
	
	public Veiculo() {
	}
	
	public Veiculo(String marca, String modelo, String placa, String chassi, int valor) {
	     this.marca = marca;
	     this.modelo = modelo;
	     this.placa = placa;
	     this.chassi = chassi;
	     this.valor = valor;
	}
	
	public String getMarca() {
	     return marca;
	}
	public void setMarca(String marca) {
	     this.marca = marca;
	}
	public String getModelo() {
	     return modelo;
	}
	public void setModelo(String modelo) {
	     this.modelo = modelo;
	}
	public String getPlaca() {
	     return placa;
	}
	public void setPlaca(String placa) {
	     this.placa = placa;
	}
	public String getChassi() {
	     return chassi;
	}
	public void setChassi(String chassi) {
	     this.chassi = chassi;
	}
	public int getValor() {
	     return valor;
	}
	public void setValor(int valor) {
	     this.valor = valor;
	}
	
	//lista todos os veiculos que sÃÂ£o VW.
	public List<Veiculo> listaVW(List<Veiculo> veiculos) {
	     return veiculos.stream()
	     .filter(veiculo -> veiculo.getMarca().toUpperCase().equals("VW"))
	     .collect(Collectors.toList());
	}
	
	//retorna se todos sÃÂ£o ou nÃÂ£o marca VW.
	public boolean todosVW(List<Veiculo> veiculos) {
	     return veiculos.stream()
	     .allMatch(veiculo -> veiculo.getMarca().toUpperCase().equals("VW"));
	}
	
	//nenhum modelo GOL
	public boolean nenhumGOL(List<Veiculo> veiculos) {
		return veiculos.stream()
		.noneMatch(veiculo -> veiculo.getModelo().toUpperCase().equals("GOL"));	
	}
	
	//retorna se algum modelo ÃÂ© JETTA.
	public boolean algumJetta(List<Veiculo> veiculos) {
		return veiculos.stream()
		.anyMatch(veiculo -> veiculo.getModelo().toUpperCase().equals("JETTA"));
	}
	
	//soma dos valores dos veiculos
	public Integer valorVeiculos(List<Veiculo> veiculos) {
		return veiculos.stream()
		.map(Veiculo::getValor)
		.reduce(0, Integer::sum);
	}

	//setando todos com placa = "ABC1234"
	public List<Veiculo> setaPlacas(List<Veiculo> veiculos) {
		return veiculos.stream()
		.peek(veiculo -> veiculo.setPlaca("ABC1234"))
		.collect(Collectors.toList());
	}

	//duplique os veÃ­Â­culos da lista.
	public List<Veiculo> duplicaVeiculos(List<Veiculo> veiculos) {
		return veiculos.stream()
		.flatMap(veiculo -> Arrays.asList(veiculo, veiculo).stream())
		.collect(Collectors.toList());
	}

	//retorna placa dos veiculos 
	public List<String> listaPlacas(List<Veiculo> veiculos) {
		return veiculos.stream()
		.flatMap(veiculo -> Arrays.asList(veiculo.getPlaca()).stream())
		.collect(Collectors.toList());
	}

	//retorna veiculos por preÃ§o 
	public List<Veiculo> listaVeiculosPorValor(List<Veiculo> veiculos) {
		Comparator<Veiculo> comparador = Comparator.comparing(Veiculo::getValor).reversed();
		return veiculos.stream()
		.sorted(comparador)
		.collect(Collectors.toList());
	}

	// EXERCICIOS ADICIONAIS

	//lista carros com valor maior que 50 mil
	public List<Veiculo> maisQueCinquenta(List<Veiculo> veiculos) {
		return veiculos.stream()
		.filter(veiculo -> (veiculo.getValor() >= 50000))
		.collect(Collectors.toList());
	}

	//busca placa específica
	public Optional<Veiculo> buscaPorPlaca(List<Veiculo> veiculos, String placa) {
		return veiculos.stream()
		.filter(veiculo -> veiculo.getPlaca().equals(placa))
		.findAny();
	}
}