package exerciciosStream;

import java.util.ArrayList;
import java.util.List;

public class Main {
     public static void main(String args[]) {

     Veiculo veiculo1 = new Veiculo();
     List<Veiculo> lista1 = new ArrayList<>();

     lista1.add(new Veiculo("VW", "GOL", "IVV9898", "65ds5ds51546156", 20000));
     lista1.add(new Veiculo("BMW", "M3", "LMN6523", "dd4f496d54f96d4", 520000));
     lista1.add(new Veiculo("VW", "JETTA", "FOF6589", "54d546sd1v65ssd", 50000));

     System.out.println("-----------------------------------");
     System.out.println("Listando os modelos VW");
     System.out.println(veiculo1.listaVW(lista1)); 
     System.out.println("-----------------------------------");
     System.out.println("Todos os modelos são VW?");
     System.out.println(veiculo1.todosVW(lista1));
     System.out.println("-----------------------------------");
     System.out.println("Algum modelo é Jetta?");
     System.out.println(veiculo1.algumJetta(lista1));
     System.out.println("-----------------------------------");
     System.out.println("Nenhuem dos modelos é GOL?");
     System.out.println(veiculo1.nenhumGOL(lista1));
     System.out.println("-----------------------------------");
     System.out.println("Qual o valor total dos veículos?");
     System.out.println(veiculo1.valorVeiculos(lista1));
     System.out.println("-----------------------------------");
     System.out.println("Veículo encontrado com a placa ");
     System.out.println(veiculo1.buscaPorPlaca(lista1, "IVV9898"));
     System.out.println("-----------------------------------");
     System.out.println("Alterando as placas para ABC1234");
     System.out.println(veiculo1.setaPlacas(lista1));
     System.out.println("-----------------------------------");
     System.out.println("Listando veículos duplicados....");
     System.out.println(veiculo1.duplicaVeiculos(lista1));
     System.out.println("-----------------------------------");
     System.out.println("Listando placas dos veículos...");
     System.out.println(veiculo1.listaPlacas(lista1));
     System.out.println("-----------------------------------");
     System.out.println("Listando veículos por valor...");
     System.out.println(veiculo1.listaVeiculosPorValor(lista1));
     System.out.println("-----------------------------------");
     System.out.println("Listando veículos mais caros que 50k...");
     System.out.println(veiculo1.maisQueCinquenta(lista1));
     
     }
}
